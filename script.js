/*Відповідь 1:
Для обробки помилок, які можуть виникнути при доступі до зовнішніх ресурсів. Наприклад, при відкритті файлу, підключенні до API або синхронізації з базою даних.
Для обробки помилок, які можуть виникнути при взаємодії з користувачем. Наприклад, при введенні невірних даних або при спробі виконати неможливу дію.
Для обробки помилок, які можуть виникнути при виконанні коду, який не повністю контролюється вами. Наприклад, при використанні сторонніх бібліотек або фреймворків.
*/

//Завдання 1:

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.getElementById("root");

if (root) {
  const ul = document.createElement("ul");

  books.forEach((book, index) => {
    const li = document.createElement("li");

    if (book.author) {
      li.textContent += `Автор: ${book.author}, `;
    }
    if (book.name) {
      li.textContent += `Назва: ${book.name}, `;
    }
    if (book.price) {
      li.textContent += `Ціна: ${book.price}`;
    }

    ul.appendChild(li);
  });

  root.appendChild(ul);
} else {
  console.error("Елемент з id='root' не знайдено.");
}
